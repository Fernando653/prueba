package pe.com.synopsis.service;

import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.beans.response.ListaUsuarioResponse;
import pe.com.synopsis.beans.response.UsuarioResponse;

public interface UsuarioService {

	public UsuarioResponse obtenerDatosUsuario(int codigo);
	
	public ListaUsuarioResponse listaUsuarios();
	
	public Integer insertarUsuario(Usuario usuario);
	
	public Integer actualizarUsuario(Usuario usuario);
	
	public Integer eliminarUsuario(int codigo);
	
	
}
