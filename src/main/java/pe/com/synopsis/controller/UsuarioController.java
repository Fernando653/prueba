package pe.com.synopsis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.com.synopsis.beans.Usuario;
import pe.com.synopsis.beans.response.ListaUsuarioResponse;
import pe.com.synopsis.beans.response.UsuarioResponse;
import pe.com.synopsis.service.UsuarioService;

@RestController
@RequestMapping("/training")
public class UsuarioController 
{
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping(value="/datosUsuario")
	public ResponseEntity<UsuarioResponse> obtenerDatosUsuario(@RequestParam(value="codigoUsuario") int codigo)
	{
		return new ResponseEntity<UsuarioResponse>(usuarioService.obtenerDatosUsuario(codigo), HttpStatus.OK);
	}
	
	@GetMapping(value="/listaUsuarios")
	public ResponseEntity<ListaUsuarioResponse> listadeUsuarios()
	{
		return new ResponseEntity<ListaUsuarioResponse>(usuarioService.listaUsuarios(), HttpStatus.OK);
	}
	
	@PostMapping(value="/insertarUsuario")
	public ResponseEntity<Integer> insertarUsuario(@RequestBody Usuario usuario)
	{
		return new ResponseEntity<Integer>(usuarioService.insertarUsuario(usuario), HttpStatus.OK);
	}
	
	@DeleteMapping(value="/eliminarUsuario")
	public ResponseEntity<Integer> eliminarUsuario(@RequestParam(value="codigoUsuario") int codigo)
	{
		return new ResponseEntity<Integer>(usuarioService.eliminarUsuario(codigo), HttpStatus.OK);
	}
	
	@PutMapping(value="/actualizarUsuario")
	public ResponseEntity<Integer> actualizarUsuario(@RequestBody Usuario usuario)
	{
		return new ResponseEntity<Integer>(usuarioService.actualizarUsuario(usuario), HttpStatus.OK);
	}
	
}
