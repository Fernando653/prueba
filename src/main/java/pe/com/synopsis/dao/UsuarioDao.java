package pe.com.synopsis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.synopsis.beans.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Integer> 
{
	
}
