package pe.com.synopsis.beans.response;

import java.util.List;

import pe.com.synopsis.beans.Usuario;

public class ListaUsuarioResponse {

	public List<Usuario> listaUsuario;

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
	
	
	
}
