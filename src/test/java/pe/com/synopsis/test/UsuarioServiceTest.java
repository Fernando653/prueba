package pe.com.synopsis.test;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import pe.com.synopsis.beans.response.ListaUsuarioResponse;
import pe.com.synopsis.beans.response.UsuarioResponse;
import pe.com.synopsis.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest 
{
	@Autowired
	UsuarioService usuarioService;
	
	@Test
	public void primeraPrueba()
	{
		UsuarioResponse  response = usuarioService.obtenerDatosUsuario(3);
		Assertions.assertNotNull(response.getUsuario());
	}
	
	
	@Test
	public void segundaPrueba()
	{
		ListaUsuarioResponse response = usuarioService.listaUsuarios();
		Assertions.assertNotNull(response.listaUsuario);
		Assertions.assertEquals(4, response.listaUsuario.size());
		Assertions.assertEquals("saul", response.listaUsuario.get(0).getNombre());
		
	}
}
